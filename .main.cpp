#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "ksquares.h"

int char_to_dir(char c)
{
    switch (tolower(c)) {
        case 'u': return UP;
        case 'd': return DOWN;
        case 'l': return LEFT;
        case 'r': return RIGHT;
    }
    return -1;
}

int input(char *array)
{
    int i = 0;
    char c;
    while ((c = fgetc(stdin)) != '\n')
        array[i++] = c;
    return i;

}

int main()
{
   
    int num_rows, num_cols; 
    int scanf_result;
    do {
        char buffer[128]; memset(buffer, 0, 128);
        puts("Enter <Num Rows> <Num Cols>");
        input(buffer);
        scanf_result = sscanf(buffer, "%d %d\n", &num_rows, &num_cols);
  
    } while (scanf_result != 2 && puts("Error: Invalid Input"));


    Game game;
    Game_init(&game, num_rows, num_cols, 2);
        
    game.players[0].str = "1";
    game.players[1].str = "2";

   
    while (Game_is_active(&game)) {

        char *str = Board_string(&game.board);
        puts(str);
        free(str);

        printf("Player %s Move\n", game.players[game.turn].str);
        
        if (game.turn == 0) {

            int row, col;
            int result;
            do {
                char buffer[128]; memset(buffer, 0, 128);    
                puts("Enter Row Col: ");

                input(buffer);
                result = sscanf(buffer, "%d %d\n", &row, &col);
            } while (result != 2 && puts("Error: Invalid Input"));
            
            int direction;
            
            do {
                char c;
                char buffer[128]; memset(buffer, 0, 128);
                puts("Enter Direction (L R U D)");
                input(buffer);
                
                result = sscanf(buffer, "%c", &c);
                direction = char_to_dir(c);
            } while ((result != 1 || direction < 0) && puts("Error: Invalid Input"));

            Game_make_move(&game, row, col, (Dir) direction); 

        }

        else {
            int move[3];
            Game_get_computer_move(&game,move);
            Game_make_move(&game, move[0], move[1], (Dir) move[2]); 
        }
        
    }
}
