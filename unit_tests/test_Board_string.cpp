#include <string.h>
#include "test_utils.hpp"
#include "ksquares.h"

int main(const int argc, const char **argv)
{
    return test_wrapper(argc, argv, []() {
        
            
            const char *expected =
                "\n" 
                "o   o   o\n"
                "  x | x |\n"
                "o---o---o\n"
                "| x   x |\n"
                "o   o---o";
            
            Board board;
            Board_init(&board, 2, 2);
            
            board.data[0][0].right->val = true;
            board.data[0][1].right->val = true;
            board.data[0][1].left->val = true;
            board.data[0][1].down->val = true;
            board.data[1][0].up->val = true;
            board.data[1][0].left->val = true;
            board.data[1][1].right->val = true;
            board.data[1][1].down->val = true;

            const char *result = Board_string(&board);
            
            return strcmp(result, expected) == 0;
     });
}
