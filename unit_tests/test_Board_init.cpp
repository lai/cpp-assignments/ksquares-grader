#include "test_utils.hpp"
#include <assert.h>
#include "ksquares.h"
#include "string.h"
#include <signal.h>

int main(const int argc, const char **argv)
{
    return test_wrapper(argc, argv, []() {
            
            Board board;
            Board_init(&board, 2, 2);
              
            assert(board.data);            
            assert(board.data[0][0].right == board.data[0][1].left);
            assert(board.data[0][0].down == board.data[1][0].up);
            assert(board.data[0][1].down == board.data[1][1].up);
            assert(board.data[1][0].right == board.data[1][1].left);
            
            Board_init(&board, 5, 5);
            for (int i = 0; i < 5; i++) {
                assert(board.data[i]);
                for (int j = 0; j < 5; j++)
                    assert(board.data[i][j].up);
            }
            
            return true;
     });
}
