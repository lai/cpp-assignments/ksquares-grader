#ifndef KSQUARES_H
#define KSQUARES_H

/* extern C is necessary to prevent name mangling. This allows us to grep 
 * for the mutated symbol after calling objdump
 * */
#ifdef __cplusplus
extern "C" {
#endif

 
#include <stdio.h>
#include <stdbool.h>

/*
 * @brief The LinkNode object represents the connection between boxes
 * Multiple boxes can share the same LinkNode object
 * o   o   o
 *   A | B 
 * o   o   o 
 *
 * Box A and Box B share the same LinkNode
 * */
typedef struct LinkNode{
        int val; // Whether or not a move has been made
        struct LinkNode *next; // When the board is destroyed, follow the links to free the LinkNode
} LinkNode;


typedef struct Box {
        LinkNode *left;
        LinkNode *up;
        LinkNode *right;
        LinkNode *down;
        /* If the Box is complete, the owner is set to the player character */
        const char *owner;
} Box;

typedef enum { 
    LEFT, 
    RIGHT, 
    UP,
    DOWN, 

    LRUD // Iterate over direction
} Dir;

/**
 * @brief Given the direction as an enumerated value, set the member in the struct
 * 
 * Input: Nothing
 * Prints: Nothing
 * Calls: Nothin
 * Returns 0 on success or -1 on Failure (ie, wrong enum value)
 * */
int Box_setitem(Box *self, Dir direction, bool value);


/*
 * Given the direction as an enumerated value, get the direction value
 * 
 * Input: Nothing
 * Prints: Nothing
 * Returns: Bool
 * Modifies: Nothing
 * Calls: Nothing
 *
 * */
bool Box_getitem(Box *self, Dir direction);



/*
 * @brief Board object definition
 * */
typedef struct Board {
        int n_rows;
        int n_cols;
        Box **data;     // 2D array of Boxes
        LinkNode *head; // Points to the first link: Only relevant during Board destruction
} Board;


/*
 * @brief Retrieve the string representation of the board 
 * Input: Nothing
 * Prints: Nothing
 * Returns: Pointer to buffer holding the string representation. The area of memory should be freed
*/
char *Board_string(Board *self);

/*
 * @brief Creates an empty board of the specified size and populate with links
 * Input: Nothing
 * Prints: Nothing
 * Returns: 0 if successful, -1 if failure.
 */
int Board_init(Board *self, int n_rows, int n_cols);


/*
 * @brief Destroy the board, freeing all memory
 * Input: Nothing
 * Returns: Nothing
 *
 */
void Board_destroy(Board *self);


/*
 * Player object
 * */
typedef struct Player {
        const char *str; // Player as a single character string
        bool is_human;   // Computer or Human?
        int score;       // How many boxes has this player completed?
} Player;


typedef struct Game {
        Board board;     // Extends Board object
        int turn;        // Running counter of the turn
        int n_players;   // Number of players
        Player *players; // All the players stored as an array
        
} Game;

/*
 * @brief Initializes an instance of a Game
 * 
 * Input: Nothing
 * Prints: Nothing
 * Returns: 0 if successful, -1 otherwise
 * Modifies: Game object as self (or this)
 * 
 * */
int Game_init(Game *self, int n_row, int n_cols, int n_players);

/*
 * @ brief 
 * */
int Game_make_move(Game *self, int n_row, int n_col, Dir direction);
int Game_is_active(Game *self);


int Game_get_computer_move(Game *self, int move[3]);
int Game_evaluate_move(Game *self, int row, int col, Dir direction);

#ifdef __cplusplus
}
#endif
#endif
