all: main.o
	g++ *.cpp main.o -o main.out

.PHONY: unit_tests mutants



unit_tests:
	rm -f main.o
	bash build_unit_tests.sh

mutants:
	rm -f main.o
	bash build_mutants.sh

main.o: .main.cpp
	g++ -c $< -o $@

clean:
	rm *.o
	rm *.out
	rm *.mut

