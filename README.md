# pa07 Think inside the box
![](think.png)

To ![think outside the box](https://en.wikipedia.org/wiki/Thinking_outside_the_box), first you must learn to think inside the box!

## Dots and Boxes
Dots and boxes is a classic mathy game.
To see how to play, read this diagram left to right, row by row:

![](dots-and-boxes.jpg)

Read and play around with the following, before you start your coding!
* https://en.wikipedia.org/wiki/Dots_and_Boxes
* https://www.math.ucla.edu/~tom/Games/dots&boxes.html
* https://www.math.ucla.edu/~tom/Games/dots&boxes_hints.html
* http://dotsandboxes.org/
* https://www.coolmathgames.com/0-dots-and-boxes
* `sudo dnf/zypper/apt install ksquares` to play a polished GUI version of this game.

## Code
* We give you the main function, and basic board setup.
* You have to write the brains inside the box!
* As always, attempt to program the functions in the order we give them.
* Do not attempt to program the second funtion until the first one is passing, or the third until the second is passing, etc.
