#include <stdlib.h>
#include <string.h>
#include "ksquares.h"

int Box_setitem(Box *self, Dir direction, bool value)
{
    switch (direction) {
        case LEFT:
                self->left->val = value;
                break;
        case RIGHT: 
                self->right->val = value;
                break; 
        case UP:
                self->up->val = value;
                break;
        case DOWN: 
                self->down->val = value;
                break;
        case LRUD: break;
    }
    return 0;
}

bool Box_getitem(Box *self, Dir direction)    
{
    switch (direction) {
        case LEFT:
            return self->left->val;
        case RIGHT: 
            return self->right->val;
        case UP:
           return self->up->val;
        case DOWN: 
           return self->down->val;
        
        case LRUD: 
            break;
    }
    return 0;
}

int Box_complete(Box *self)
{
    return self->up->val && self->down->val && self->right->val && self->left->val;
}

static LinkNode *new_link(Board *self)
{
    LinkNode *node = (LinkNode *) malloc(sizeof(LinkNode));
    // Push the new node onto the stack. Makes it easier to destroy later
    node->next = self->head;
    self->head = node;
    return node;
}

int Board_init(Board *self, int n_rows, int n_cols)
{
    self->n_rows = n_rows;
    self->n_cols = n_cols;
    self->head = NULL;
    // Init array
    Box **data = self->data = (Box **) calloc(n_rows, sizeof(Box *));
    for (int i = 0; i < n_rows; i++) {
        data[i] = (Box *) calloc(n_cols, sizeof(Box));
    }

    // populate links
    for (int i = 0; i < n_rows; i++) {
        for (int j = 0; j < n_cols; j++) {
            if (i > 0) {
                data[i][j].up = data[i-1][j].down;    
            }
            else {
                data[i][j].up = new_link(self);
            }
            if (j > 0) {
                data[i][j].left = data[i][j-1].right;
            }
            else {
                data[i][j].left = new_link(self);
            }
            data[i][j].down = new_link(self);
            data[i][j].right = new_link(self);
            data[i][j].owner = "x";
           
             
        }
    }
    return 0;
}

char *Board_string(Board *self)
{
    Box **board = self->data;

    char *array = (char *)calloc(10 * self->n_cols * self->n_rows, 1); 
    array[0] = '\n';

    for (int i = 0; i < self->n_rows; i++) {
        char *rowstr = (char *) calloc(10, self->n_cols);
        char *rowstrB = (char *) calloc(10, self->n_cols);
            
        
        for (int j = 0; j < self->n_cols; j++) {        
           
            strcat(rowstr, "o");
            if (board[i][j].up->val) {
                strcat(rowstr, "---");
            }
            else {
                strcat(rowstr, "   ");
            }

            if (board[i][j].left->val) {
                strcat(rowstrB, "| ");
                strcat(rowstrB, board[i][j].owner);
                strcat(rowstrB, " ");
            }
            else {
                strcat(rowstrB, "  ");
                strcat(rowstrB, board[i][j].owner);
                strcat(rowstrB, " ");
            }
        }
       
        int col = self->n_cols - 1; 
        if (board[i][col].right->val) {
            strcat(rowstrB, "|");
        }
        else {
            strcat(rowstrB, " ");
        }

       
        strcat(rowstr, "o"); 
        strcat(rowstr, "\n");
        strcat(rowstrB, "\n");
        strcat(array, rowstr);
        strcat(array, rowstrB);
        free(rowstr);
        free(rowstrB);
    }
    
    char *rowstr = (char *) calloc(5, self->n_cols);
      
    for (int j = 0; j < self->n_cols; j++) {
        strcat(rowstr, "o");
        if (board[self->n_rows - 1][j].down->val)
        strcat(rowstr, "---");
        else
            strcat(rowstr, "   ");
    }
    strcat(rowstr, "o");
    strcat(array, rowstr);
    free(rowstr);
    return array;
}


void Board_destroy(Board *self)
{
    LinkNode *iter = self->head;
    while (iter) {
        LinkNode *node = iter;
        iter = iter->next;
        free(node);
    }
    for (int i = 0; i < self->n_rows; i++)
        free(self->data[i]);
    free(self->data);
}

int Game_init(Game *self, int n_row, int n_cols, int n_players)
{
    Board_init(&self->board, n_row, n_cols);
    self->players = (Player *) calloc(n_players, sizeof(Player));
    self->turn = 0;
    self->n_players = n_players;
    return 0;
}

int Game_make_move(Game *self, int row, int col, Dir direction)
{
    Board *board = &self->board;
    Box *box = &board->data[row][col];
    Player *player = &self->players[self->turn];
   
    if (Box_getitem(box, direction))
        return -1; // Already moved
    
    Box_setitem(box, direction, true);
    if (Box_complete(box)) {
        box->owner = player->str;
        player->score ++;
    }
    else {
        // next player
        self->turn = (self->turn + 1) % self->n_players;
    }
    return 0;
}


int Game_is_active(Game *self)
{
    int sum = 0;
    for (int i = 0; i < self->n_players; i++) {
        sum += self->players[i].score;
    }
    return sum < (self->board.n_cols * self->board.n_rows);

}


static int Box_count_wall(Box *self)
{
        int count = 0;
        if (self->down->val) count ++;
        if (self->up->val) count ++;
        if (self->right->val) count ++;
        if (self->left->val) count ++;
        return count;
}


int Game_evaluate_move(Game *self, int row, int col, Dir direction)
{
    int dirs[][2] = {
        {0, -1},
        {0, 1},
        {-1, 0},
        {1, 0}
    };
    
    int neighbor_row = row + dirs[direction][0];
    int neighbor_col = row + dirs[direction][1];
    int neighbor = ((0 <= neighbor_row && neighbor_row < self->board.n_rows) 
            && (0 <= neighbor_col && neighbor_col < self->board.n_cols)); 
    
   int wall_count = Box_count_wall(& self->board.data[row][col]);
   int neighbor_wall_count = neighbor ? Box_count_wall(& self->board.data[ neighbor_row ][ neighbor_col ])  : 3;
   
   int scores[] = { 0, 1, 2, -3 };
   if (Box_getitem(& self->board.data[row][col], direction))
      return 3; // Taken
   int score = scores[wall_count];
   int neighbor_score = neighbor ? scores[neighbor_wall_count] : -1;
   if (score == -1)
     return score;
   return score >= neighbor_score ? score : neighbor_score;
}

int Game_get_computer_move(Game *self, int move[3])
{     
       int moveEval = 3;
       for (int i = 0; i < self->board.n_rows; i++) {
           for (int j = 0; j < self->board.n_cols; j++) {
               for (int dir = 0; dir < LRUD; dir++) {
                   int temp_eval = Game_evaluate_move(self, i, j,(Dir) dir);
                   if (temp_eval < moveEval) {
                        moveEval = temp_eval;
                        move[0] = i;
                        move[1] = j;
                        move[2] = dir;
                   }
               }
           }
       }
       return 0;
}
